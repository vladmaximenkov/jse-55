package ru.vmaksimenkov.tm;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vmaksimenkov.tm.endpoint.SessionRecord;
import ru.vmaksimenkov.tm.endpoint.Status;
import ru.vmaksimenkov.tm.endpoint.TaskRecord;
import ru.vmaksimenkov.tm.marker.SoapCategory;

import javax.xml.ws.WebServiceException;

public class TaskEndpointTest extends AbstractEndpointTest {

    @NotNull
    private static final String TEST_DESCRIPTION = "Test task description";
    @NotNull
    private static final String TEST_NAME = "Test task name";
    @NotNull
    private static final String TEST_NAME_TWO = "Test task name 2";
    @NotNull
    private static TaskRecord TEST_TASK = TASK_ENDPOINT.createTask(SESSION, TEST_NAME, TEST_DESCRIPTION);
    @NotNull
    private static String TEST_TASK_ID = TEST_TASK.getId();

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void clearTasksWithInvalidSession() {
        @NotNull final SessionRecord emptySessionRecord = new SessionRecord();
        TASK_ENDPOINT.clearTask(emptySessionRecord);
    }

    @Test
    @Category(SoapCategory.class)
    public void countTask() {
        TASK_ENDPOINT.clearTask(SESSION);
        Assert.assertEquals(0, (long) TASK_ENDPOINT.countTask(SESSION));
        TASK_ENDPOINT.createTask(SESSION, TEST_NAME, TEST_DESCRIPTION);
        Assert.assertEquals(1, (long) TASK_ENDPOINT.countTask(SESSION));
        TASK_ENDPOINT.createTask(SESSION, TEST_NAME_TWO, TEST_DESCRIPTION);
        Assert.assertEquals(2, (long) TASK_ENDPOINT.countTask(SESSION));
        TASK_ENDPOINT.clearTask(SESSION);
        Assert.assertEquals(0, (long) TASK_ENDPOINT.countTask(SESSION));
    }

    @Test
    @Category(SoapCategory.class)
    public void createTest() {
        @NotNull final TaskRecord task = TASK_ENDPOINT.createTask(SESSION, TEST_NAME, TEST_DESCRIPTION);
        Assert.assertNotNull(task);
        Assert.assertEquals(TEST_NAME, task.getName());
    }

    @Test
    @Category(SoapCategory.class)
    public void finishTask() {
        TASK_ENDPOINT.finishTaskById(SESSION, TEST_TASK_ID);
        Assert.assertEquals(Status.COMPLETE, TASK_ENDPOINT.findTaskById(SESSION, TEST_TASK_ID).getStatus());
    }

    @After
    public void finishTest() {
        TASK_ENDPOINT.clearTask(SESSION);
    }

    @Test
    @Category(SoapCategory.class)
    public void startTask() {
        TASK_ENDPOINT.startTaskById(SESSION, TEST_TASK.getId());
        Assert.assertEquals(Status.IN_PROGRESS, TASK_ENDPOINT.findTaskById(SESSION, TEST_TASK_ID).getStatus());
    }

    @Before
    public void startTest() {
        TEST_TASK = TASK_ENDPOINT.createTask(SESSION, TEST_NAME, TEST_DESCRIPTION);
        TEST_TASK_ID = TEST_TASK.getId();
    }

    @Test
    @Category(SoapCategory.class)
    public void updateTask() {
        Assert.assertEquals(TEST_NAME, TEST_TASK.getName());
        TASK_ENDPOINT.updateTaskById(SESSION, TEST_TASK_ID, TEST_NAME_TWO, TEST_DESCRIPTION);
        Assert.assertEquals(TEST_NAME_TWO, TASK_ENDPOINT.findTaskById(SESSION, TEST_TASK_ID).getName());
    }

}
