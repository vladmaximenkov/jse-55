package ru.vmaksimenkov.tm;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vmaksimenkov.tm.marker.SoapCategory;

import javax.xml.ws.WebServiceException;

public class AdminEndpointTest extends AbstractEndpointTest {

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void denyLoadBackupTest() {
        SESSION = SESSION_ENDPOINT.openSession(TEST_USER_NAME, TEST_USER_PASSWORD);
        ADMIN_ENDPOINT.loadBackup(SESSION);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void denyLoadJsonTest() {
        SESSION = SESSION_ENDPOINT.openSession(TEST_USER_NAME, TEST_USER_PASSWORD);
        ADMIN_ENDPOINT.loadJson(SESSION);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void denySaveBackupTest() {
        SESSION = SESSION_ENDPOINT.openSession(TEST_USER_NAME, TEST_USER_PASSWORD);
        ADMIN_ENDPOINT.saveBackup(SESSION);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void denySaveJsonTest() {
        SESSION = SESSION_ENDPOINT.openSession(TEST_USER_NAME, TEST_USER_PASSWORD);
        ADMIN_ENDPOINT.saveJson(SESSION);
    }

    @After
    public void finishTest() {
        SESSION = SESSION_ENDPOINT.openSession(TEST_USER_NAME, TEST_USER_PASSWORD);
    }

    @Test
    @Category(SoapCategory.class)
    public void runTest() {
        ADMIN_ENDPOINT.saveJson(SESSION);
        ADMIN_ENDPOINT.loadJson(SESSION);
        ADMIN_ENDPOINT.saveBackup(SESSION);
        ADMIN_ENDPOINT.loadBackup(SESSION);
    }

    @Before
    public void startTest() {
        SESSION = SESSION_ENDPOINT.openSession(ADMIN_USER_NAME, ADMIN_USER_PASSWORD);
    }

}
