package ru.vmaksimenkov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.command.AbstractCommand;

@Component
public final class ExitCommand extends AbstractCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Close application";
    }

    @NotNull
    @Override
    public String commandName() {
        return "exit";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
