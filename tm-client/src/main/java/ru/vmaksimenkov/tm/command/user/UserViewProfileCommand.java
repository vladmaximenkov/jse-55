package ru.vmaksimenkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.endpoint.UserEndpoint;
import ru.vmaksimenkov.tm.endpoint.UserRecord;

@Component
public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    @Autowired
    protected UserEndpoint userEndpoint;

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Show user profile";
    }

    @NotNull
    @Override
    public String commandName() {
        return "user-view";
    }

    @Override
    public void execute() {
        @Nullable final UserRecord user = userEndpoint.viewUser(sessionService.getSession());
        if (user == null) return;
        showUser(user);
    }

}
