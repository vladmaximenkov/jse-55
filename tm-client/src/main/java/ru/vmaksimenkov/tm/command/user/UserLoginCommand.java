package ru.vmaksimenkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.endpoint.SessionEndpoint;
import ru.vmaksimenkov.tm.endpoint.UserEndpoint;
import ru.vmaksimenkov.tm.exception.entity.UserNotFoundException;
import ru.vmaksimenkov.tm.exception.user.AlreadyLoggedInException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import static ru.vmaksimenkov.tm.util.TerminalUtil.nextLine;

@Component
public final class UserLoginCommand extends AbstractUserCommand {

    @NotNull
    @Autowired
    protected SessionEndpoint sessionEndpoint;

    @NotNull
    @Autowired
    protected UserEndpoint userEndpoint;

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Log in to the system";
    }

    @NotNull
    @Override
    public String commandName() {
        return "user-login";
    }

    @Override
    public void execute() {
        if (sessionService.getSession() != null) throw new AlreadyLoggedInException();
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = nextLine();
        if (!userEndpoint.existsUserByLogin(login))
            throw new UserNotFoundException();
        System.out.println("ENTER PASSWORD:");
        sessionService.setSession(
                sessionEndpoint.openSession(login, TerminalUtil.nextLine())
        );
    }

}
