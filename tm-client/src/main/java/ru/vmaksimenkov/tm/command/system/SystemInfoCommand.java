package ru.vmaksimenkov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.command.AbstractCommand;
import ru.vmaksimenkov.tm.util.NumberUtil;

@Component
public final class SystemInfoCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandArg() {
        return "-i";
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Show system info";
    }

    @NotNull
    @Override
    public String commandName() {
        return "info";
    }

    @Override
    public void execute() {
        System.out.println("[INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        @NotNull final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        @NotNull final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));

    }

}
