package ru.vmaksimenkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.exception.entity.NoProjectsException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

@Component
public final class ProjectByIdFinishCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Finish project by id";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-finish-by-id";
    }

    @Override
    public void execute() {
        if (projectEndpoint.countProject(sessionService.getSession()) < 1)
            throw new NoProjectsException();
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER ID:");
        projectEndpoint.finishProjectById(sessionService.getSession(), TerminalUtil.nextLine());
    }

}
