package ru.vmaksimenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.util.TerminalUtil;

@Component
public final class TaskByNameFinishCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Finish task by name";
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-finish-by-name";
    }

    @Override
    public void execute() {
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER NAME:");
        taskEndpoint.finishTaskByName(sessionService.getSession(), TerminalUtil.nextLine());
    }

}
