package ru.vmaksimenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.util.TerminalUtil;

@Component
public final class TaskByIdRemoveCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Remove task by id";
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-remove-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER ID:");
        taskEndpoint.removeTaskById(sessionService.getSession(), TerminalUtil.nextLine());
    }

}
