package ru.vmaksimenkov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component
public final class DataJsonSaveFasterXmlCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @Nullable
    @Override
    public String commandDescription() {
        return "Save data to json file";
    }

    @NotNull
    @Override
    public String commandName() {
        return "data-json-save";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA JSON SAVE]");
        adminEndpoint.saveJson(sessionService.getSession());
    }

}
