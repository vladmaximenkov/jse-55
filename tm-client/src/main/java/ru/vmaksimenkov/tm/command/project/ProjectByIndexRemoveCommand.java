package ru.vmaksimenkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.util.TerminalUtil;

@Component
public final class ProjectByIndexRemoveCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Remove project by index";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-remove-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER INDEX:");
        projectEndpoint.removeProjectByIndex(sessionService.getSession(), TerminalUtil.nextNumber());
    }

}
