package ru.vmaksimenkov.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.command.AbstractCommand;
import ru.vmaksimenkov.tm.service.PropertyService;

@Component
public final class VersionCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected PropertyService propertyService;

    @NotNull
    @Override
    public String commandArg() {
        return "-v";
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Show version";
    }

    @NotNull
    @Override
    public String commandName() {
        return "version";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println(propertyService.getApplicationVersion());
        System.out.println("[BUILD]");
        System.out.println(Manifests.read("build"));
    }

}
