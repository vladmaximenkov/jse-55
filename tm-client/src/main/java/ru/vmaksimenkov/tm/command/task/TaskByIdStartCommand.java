package ru.vmaksimenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.util.TerminalUtil;

@Component
public final class TaskByIdStartCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Start task by id";
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-start-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[START TASK]");
        System.out.println("ENTER ID:");
        taskEndpoint.startTaskById(sessionService.getSession(), TerminalUtil.nextLine());
    }

}
