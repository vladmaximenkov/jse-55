package ru.vmaksimenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.util.TerminalUtil;

@Component
public final class TaskByIndexViewCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "View task by index";
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-view-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        showTask(taskEndpoint.findTaskByIndex(sessionService.getSession(), TerminalUtil.nextNumber()));
    }

}
