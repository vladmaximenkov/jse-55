package ru.vmaksimenkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.endpoint.Role;
import ru.vmaksimenkov.tm.endpoint.SessionEndpoint;
import ru.vmaksimenkov.tm.endpoint.SessionRecord;
import ru.vmaksimenkov.tm.exception.user.NotLoggedInException;

@Component
public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    @Autowired
    protected SessionEndpoint sessionEndpoint;

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Log out of the system";
    }

    @NotNull
    @Override
    public String commandName() {
        return "user-logout";
    }

    @NotNull
    @Override
    public Role[] commandRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        @Nullable final SessionRecord session = sessionService.getSession();
        if (session == null) throw new NotLoggedInException();
        sessionEndpoint.closeSession(session);
        sessionService.setSession(null);
    }

}
