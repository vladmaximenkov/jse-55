package ru.vmaksimenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.vmaksimenkov.tm.command.AbstractCommand;
import ru.vmaksimenkov.tm.endpoint.Role;
import ru.vmaksimenkov.tm.endpoint.TaskEndpoint;
import ru.vmaksimenkov.tm.endpoint.TaskRecord;
import ru.vmaksimenkov.tm.exception.entity.TaskNotFoundException;
import ru.vmaksimenkov.tm.service.SessionService;

import static ru.vmaksimenkov.tm.util.TerminalUtil.dashedLine;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected TaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    protected SessionService sessionService;

    @Nullable
    @Override
    public Role[] commandRoles() {
        return Role.values();
    }

    protected void showTask(@Nullable final TaskRecord task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        if (task.getStatus() != null)
            System.out.println("STATUS: " + task.getStatus());
        System.out.println("PROJECT ID: " + task.getProjectId());
        System.out.println("CREATED: " + task.getCreated());
        System.out.println("STARTED: " + task.getDateStart());
        System.out.println("FINISHED: " + task.getDateFinish());
        System.out.print(dashedLine());
    }

}
