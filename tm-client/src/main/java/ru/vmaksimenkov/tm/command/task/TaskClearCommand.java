package ru.vmaksimenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component
public final class TaskClearCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Remove all tasks";
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-clear";
    }

    @Override
    public void execute() {
        System.out.println("[TASK CLEAR]");
        taskEndpoint.clearTask(sessionService.getSession());
    }

}
