package ru.vmaksimenkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.vmaksimenkov.tm.api.service.ISessionService;
import ru.vmaksimenkov.tm.command.AbstractCommand;
import ru.vmaksimenkov.tm.endpoint.UserRecord;
import ru.vmaksimenkov.tm.exception.entity.UserNotFoundException;

import static ru.vmaksimenkov.tm.util.TerminalUtil.dashedLine;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected ISessionService sessionService;

    protected void showUser(@Nullable final UserRecord user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        if (user.getRole() != null)
            System.out.println("ROLE: " + user.getRole());
        System.out.print(dashedLine());
    }

}
