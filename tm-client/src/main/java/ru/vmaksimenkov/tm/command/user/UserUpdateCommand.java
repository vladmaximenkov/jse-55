package ru.vmaksimenkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.endpoint.UserEndpoint;
import ru.vmaksimenkov.tm.util.TerminalUtil;

@Component
public final class UserUpdateCommand extends AbstractUserCommand {

    @NotNull
    @Autowired
    protected UserEndpoint userEndpoint;

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Update user profile";
    }

    @NotNull
    @Override
    public String commandName() {
        return "user-update";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROFILE]");
        System.out.println("ENTER FIRST NAME:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        userEndpoint.updateUser(sessionService.getSession(), firstName, lastName, middleName);
    }

}
