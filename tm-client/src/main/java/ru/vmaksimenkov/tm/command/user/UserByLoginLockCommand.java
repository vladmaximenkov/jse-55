package ru.vmaksimenkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.endpoint.AdminUserEndpoint;
import ru.vmaksimenkov.tm.endpoint.Role;

import static ru.vmaksimenkov.tm.util.TerminalUtil.nextLine;

@Component
public final class UserByLoginLockCommand extends AbstractUserCommand {

    @NotNull
    @Autowired
    protected AdminUserEndpoint adminUserEndpoint;

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Lock user by login";
    }

    @NotNull
    @Override
    public String commandName() {
        return "user-lock-by-login";
    }

    @NotNull
    @Override
    public Role[] commandRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN:");
        adminUserEndpoint.lockUserByLogin(sessionService.getSession(), nextLine());
    }

}
