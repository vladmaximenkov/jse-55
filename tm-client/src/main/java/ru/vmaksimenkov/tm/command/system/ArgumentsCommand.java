package ru.vmaksimenkov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.command.AbstractCommand;
import ru.vmaksimenkov.tm.service.CommandService;

import java.util.Collection;

@Component
public final class ArgumentsCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private CommandService commandService;

    @NotNull
    @Override
    public String commandArg() {
        return "-a";
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Show all arguments";
    }

    @NotNull
    @Override
    public String commandName() {
        return "arguments";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @Nullable final Collection<AbstractCommand> arguments = commandService.getArguments();
        if (arguments == null) return;
        arguments.forEach(e -> System.out.println(e.commandArg()));
    }

}
