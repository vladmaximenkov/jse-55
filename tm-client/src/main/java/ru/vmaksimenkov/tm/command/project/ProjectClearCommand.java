package ru.vmaksimenkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component
public final class ProjectClearCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Delete all projects";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-clear";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CLEAR]");
        projectEndpoint.clearProject(sessionService.getSession());
    }

}
