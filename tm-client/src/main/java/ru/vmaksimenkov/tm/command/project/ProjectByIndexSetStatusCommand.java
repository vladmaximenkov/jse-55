package ru.vmaksimenkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.endpoint.Status;
import ru.vmaksimenkov.tm.exception.system.IndexIncorrectException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import java.util.Arrays;

import static ru.vmaksimenkov.tm.util.ValidationUtil.checkIndex;

@Component
public final class ProjectByIndexSetStatusCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Set project status by index";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-set-status-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[SET PROJECT STATUS]");
        System.out.println("ENTER INDEX:");
        final int index = TerminalUtil.nextNumber();
        if (!checkIndex(index, projectEndpoint.countProject(sessionService.getSession())))
            throw new IndexIncorrectException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        projectEndpoint.setProjectStatusByIndex(sessionService.getSession(), index, Status.valueOf(TerminalUtil.nextLine()));
    }

}
