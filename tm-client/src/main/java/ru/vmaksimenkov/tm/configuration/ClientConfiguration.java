package ru.vmaksimenkov.tm.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.vmaksimenkov.tm.endpoint.*;

@Configuration
@ComponentScan("ru.vmaksimenkov.tm")
public class ClientConfiguration {

    @Bean
    @NotNull
    public UserEndpointService userEndpointService() {
        return new UserEndpointService();
    }

    @Bean
    @NotNull
    public UserEndpoint userEndpoint(
            @NotNull @Autowired final UserEndpointService userEndpointService
    ) {
        return userEndpointService.getUserEndpointPort();
    }

    @Bean
    @NotNull
    public SessionEndpointService sessionEndpointService() {
        return new SessionEndpointService();
    }

    @Bean
    @NotNull
    public SessionEndpoint sessionEndpoint(
            @NotNull @Autowired final SessionEndpointService sessionEndpointService
    ) {
        return sessionEndpointService.getSessionEndpointPort();
    }

    @Bean
    @NotNull
    public TaskEndpointService taskEndpointService() {
        return new TaskEndpointService();
    }

    @Bean
    @NotNull
    public TaskEndpoint taskEndpoint(
            @NotNull @Autowired final TaskEndpointService taskEndpointService
    ) {
        return taskEndpointService.getTaskEndpointPort();
    }


    @Bean
    @NotNull
    public ProjectEndpointService projectEndpointService() {
        return new ProjectEndpointService();
    }

    @Bean
    @NotNull
    public ProjectEndpoint projectEndpoint(
            @NotNull @Autowired final ProjectEndpointService projectEndpointService
    ) {
        return projectEndpointService.getProjectEndpointPort();
    }

    @Bean
    @NotNull
    public AdminEndpointService adminEndpointService() {
        return new AdminEndpointService();
    }

    @Bean
    @NotNull
    public AdminEndpoint adminEndpoint(
            @NotNull @Autowired final AdminEndpointService adminEndpointService
    ) {
        return adminEndpointService.getAdminEndpointPort();
    }

    @Bean
    @NotNull
    public AdminUserEndpointService adminUserEndpointService() {
        return new AdminUserEndpointService();
    }

    @Bean
    @NotNull
    public AdminUserEndpoint adminUserEndpoint(
            @NotNull @Autowired final AdminUserEndpointService adminUserEndpointService
    ) {
        return adminUserEndpointService.getAdminUserEndpointPort();
    }

}
