package ru.vmaksimenkov.tm.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.bootstrap.Bootstrap;
import ru.vmaksimenkov.tm.service.PropertyService;

import java.io.File;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public class FileScanner implements Runnable {

    @NotNull
    private final static String FILE_DIRECTORY = "./";
    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();
    @NotNull
    @Autowired
    protected Bootstrap bootstrap;
    @NotNull
    @Autowired
    protected PropertyService propertyService;

    public void init() {
        es.scheduleWithFixedDelay(this, 0, propertyService.getScannerInterval(), TimeUnit.SECONDS);
    }

    public void run() {
        @NotNull final File file = new File(FILE_DIRECTORY);
        for (@NotNull final File item : file.listFiles()) {
            if (!item.isFile()) continue;
            @NotNull final String fileName = item.getName();
            final boolean check = bootstrap.getCommands().containsKey(fileName);
            if (!check) continue;
            bootstrap.parseCommand(fileName);
            item.delete();
        }
    }

    public void stop() {
        es.shutdown();
    }

}
