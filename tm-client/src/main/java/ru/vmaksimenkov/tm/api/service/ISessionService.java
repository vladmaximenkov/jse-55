package ru.vmaksimenkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.endpoint.SessionRecord;

public interface ISessionService {

    @Nullable SessionRecord getSession();

    void setSession(@NotNull SessionRecord currentSession);

}
