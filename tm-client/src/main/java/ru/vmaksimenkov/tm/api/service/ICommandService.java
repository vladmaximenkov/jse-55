package ru.vmaksimenkov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    @Nullable
    Collection<AbstractCommand> getArguments();

}
