package ru.vmaksimenkov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.listener.EntityListener;

import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
@EntityListeners(EntityListener.class)
public class AbstractEntityRecord implements Serializable {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

}
