package ru.vmaksimenkov.tm.repository.model;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.repository.model.IUserRepository;
import ru.vmaksimenkov.tm.model.User;

import javax.persistence.EntityManager;
import java.util.List;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final EntityManager em) {
        super(em, User.class);
    }

    @Override
    public void clear() {
        em.createQuery("DELETE User").executeUpdate();
    }

    @Override
    public boolean existsByEmail(@Nullable final String email) {
        return em.createQuery("SELECT COUNT(*) FROM User WHERE email = :email", Long.class)
                .setParameter("email", email)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return em.createQuery("SELECT COUNT(*) FROM User WHERE id = :id", Long.class)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsByLogin(@Nullable final String login) {
        return em.createQuery("SELECT COUNT(*) FROM User WHERE login = :login", Long.class)
                .setParameter("login", login)
                .getSingleResult() > 0;
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return em.createQuery("FROM User", User.class)
                .setHint(QueryHints.HINT_CACHEABLE, true).getResultList();
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        return getEntity(em.createQuery("FROM User WHERE login = :login", User.class)
                .setParameter("login", login)
                .setMaxResults(1));
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        em.createQuery("DELETE User WHERE login = :login")
                .setParameter("login", login).executeUpdate();
    }

    @Override
    public Long size() {
        return em.createQuery("SELECT COUNT(*) FROM User", Long.class)
                .setHint(QueryHints.HINT_CACHEABLE, true).setMaxResults(1).getSingleResult();
    }

}
